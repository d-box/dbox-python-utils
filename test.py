import asyncio
import logging
import shlex

from config_watch import simple_run, run_and_get_output, simple_shell_escape


async def test():
    ssid = 'K&G'
    passwd = '123456789'
    # res = await simple_run(f'/usr/bin/wpa_passphrase {ssid} > /tmp/wpa_supplicant.conf', passwd)
    res = await run_and_get_output(('/usr/bin/wpa_passphrase', ssid, passwd))
    print(res)


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s')

loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(test())
except KeyboardInterrupt as e:
    print(e)
finally:
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()
