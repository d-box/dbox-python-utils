#!/usr/bin/env python3
import sys
import time

import serial
import usb.core


def detect_usb_device() -> bool:
    try:
        dev = usb.core.find(idVendor=0x05c6, idProduct=0x90b2)
        if dev is not None:
            return True
    except Exception as e:
        print(f'Could not find USB device, error: {e}', file=sys.stderr)
    return False


def try_configure_via_serial(path: str) -> bool:
    commands = [
        ('+++', None, 'OK', False),
        ('ATP1', '7', 'OK', True),
        ('ATP1', None, '7', True),
        # ('ATP0', '6', 'OK', True),
        # ('ATP0', None, '6', True)
        ('ATDO', '4', 'OK', True),
        ('ATDO', None, '4', True),
        ('ATWR', None, 'OK', True),
        ('ATFR', None, 'OK', True),
    ]
    with serial.Serial(path, 9600, timeout=3) as port:
        for cmd in commands:
            while port.in_waiting:
                port.read()

            if cmd[1] is not None:
                string = f'{cmd[0]} {cmd[1]}'.encode('ascii', 'ignore')
            else:
                string = f'{cmd[0]}'.encode('ascii', 'ignore')

            print(f'Sending {string}')
            port.write(string)

            if cmd[3]:
                port.write('\r'.encode('ascii', 'ignore'))
            try:
                ret = port.read(len(cmd[2])).decode('ascii')
            except Exception as e:
                print(f'Could not read from serial port, error: {e}', file=sys.stderr)
                ret = ''
            print(f'Read: {ret}')
            ret = ret.strip()
            if ret != cmd[2].strip():
                return False
        return True


if __name__ == '__main__':
    SERIAL_CONSOLE = '/dev/ttySC0'
    while not detect_usb_device():
        if not try_configure_via_serial(SERIAL_CONSOLE):
            time.sleep(20)
        time.sleep(10)
