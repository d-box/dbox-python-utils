from enum import Enum

WIFI_FILE_NAME = 'wifi'
ETH_FILE_NAME = 'ethernet'
GSM_FILE_NAME = 'gsm'


class ConfigFileType(Enum):
    WIFI = WIFI_FILE_NAME
    ETH = ETH_FILE_NAME
    GSM = GSM_FILE_NAME


class ValueWithValidation:
    def __init__(self, initial, valid=None):
        self.initial = initial
        self.valid = valid

    def validate(self, value):
        if self.valid is None or not len(self.valid):
            return True

        if value not in self.valid:
            return False
        return True

    def default(self):
        return self.initial


default_wifi_config = {
    'UP': ValueWithValidation('1', ('0', '1')),
    'MODE': ValueWithValidation('AP', ('AP', 'STA')),
    'SSID': ValueWithValidation('K&G'),
    'PASSWD': ValueWithValidation('K&G_32gh'),
    'DHCP': ValueWithValidation('1', ('0', '1')),
    'IP': ValueWithValidation(''),
    'NETMASK': ValueWithValidation(''),
}

default_eth_config = {
    'UP': ValueWithValidation('1', ('0', '1')),
    'DHCP': ValueWithValidation('1', ('0', '1')),
    'IP': ValueWithValidation(''),
    'NETMASK': ValueWithValidation(''),
}

default_gsm_config = {
    'UP': ValueWithValidation('1', ('0', '1')),
    'APN': ValueWithValidation(''),
}
