from asyncio import Queue, Future, FIRST_COMPLETED, shield, TimeoutError, StreamWriter, ensure_future, wait_for, wait, \
    create_subprocess_exec, subprocess
import logging


logger = logging.getLogger(__name__)


class ExternalProcess:
    def __init__(self, command, loop, output_process_function):
        super().__init__()
        self.command = command
        self._process = None
        self._out_queue = None
        self.output_process_function = output_process_function

        self._restarting = False
        self._shutting_down = False

        self.loop = loop

        self._finish_future = None
        self._handle_in_finished_future = None
        self._handle_out_finished_future = None

    def _init_queue(self):
        self._out_queue = Queue(loop=self.loop)

    def start(self):
        ensure_future(self._spawn_process())

    async def stop(self):
        logger.info('Stopping {}'.format(self.__class__.__name__))
        await self._handle_shutdown()

    async def _spawn_process(self):
        try:
            self._process = await create_subprocess_exec(
                    *self.command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, loop=self.loop
            )
        except Exception as e:
            logger.exception('Could not start Recorder: %s', e)
            return
        self._finish_future = Future(loop=self.loop)
        self._handle_in_finished_future = Future(loop=self.loop)
        self._handle_out_finished_future = Future(loop=self.loop)

        ensure_future(self._handle_in(self._process.stdout))
        ensure_future(self._handle_out(self._process.stdin))

    async def kill_process(self):
        if self._process:
            try:
                self._process.kill()
            except ProcessLookupError:
                pass
            try:
                await wait_for(self._process.wait(), timeout=1, loop=self.loop)
            except TimeoutError as e:
                # TODO: failure to terminate means zombie process. reboot required?
                logger.error('Exception in {}: Timeout on killing pocess {}. {}'.format(
                    self.__class__.__name__, self._process.pid, e)
                )
                try:
                    self._process.kill()
                except ProcessLookupError:
                    pass

    async def force_restart(self):
        if self._restarting or self._shutting_down:
            return
        self._restarting = True
        try:
            await self.kill_process()
        except Exception as e:
            logger.error('Exception {} in {}.force_restart: {}'.format(e.__class__.__name__, self.__class__.__name__, e))
        ensure_future(self._handle_restart())

    async def _cleanup(self):
        if not self._finish_future.done():
            self._finish_future.set_result(True)
        await self._handle_out_finished_future
        await self._handle_in_finished_future
        try:
            await self.kill_process()
        except Exception as e:
            logger.error('Exception {} in {}._cleanup: {}'.format(e.__class__.__name__, self.__class__.__name__, e))

    async def _handle_shutdown(self):
        self._shutting_down = True
        self._reject_call_from_tasks = True

        await self._cleanup()

    async def _handle_restart(self):
        if self._shutting_down:
            return
        self._restarting = True
        self._reject_call_from_tasks = True
        await self._cleanup()

        self._init_queue()

        await self._spawn_process()
        self._restarting = False

    async def _handle_in(self, reader):
        try:
            read_line_task = None
            while not self._finish_future.done():
                if read_line_task is None:
                    read_line_task = ensure_future(reader.readline())
                done, pending = await wait([read_line_task, self._finish_future], return_when=FIRST_COMPLETED, timeout=30)
                if not done:
                    ensure_future(self._handle_restart())
                    break
                if read_line_task in done:
                    line = read_line_task.result()
                    if not line:
                        if not self._restarting and not self._finish_future.done() and not self._shutting_down:
                            logger.warning('{} has finished - restarting'.format(self.__class__.__name__))
                            ensure_future(self._handle_restart())
                        break
                    try:
                        if not self._restarting and not self._finish_future.done() and not self._shutting_down:
                            if self.output_process_function is not None:
                                try:
                                    self.output_process_function(line.decode())
                                except Exception as e:
                                    logger.exception('Failed handling output for process: %s', e)
                            else:
                                logger.debug(line.decode())
                    except Exception as e:
                        if not self._restarting and not self._finish_future.done() and not self._shutting_down:
                            logger.exception('{} error processing input - restarting: {}'.format(
                                self.__class__.__name__, e))
                            ensure_future(self._handle_restart())
                        break
                    else:
                        read_line_task = None
                if self._finish_future in done:
                    for read_line_pending_task in pending:
                        read_line_pending_task.cancel()
                    break
        finally:
            if not self._handle_in_finished_future.done():
                self._handle_in_finished_future.set_result(True)

    async def _handle_out(self, writer: StreamWriter):
        try:
            while not self._finish_future.done():
                to_send_task = ensure_future(self._out_queue.get())
                done, pending = await wait([to_send_task, self._finish_future], return_when=FIRST_COMPLETED)
                if to_send_task in done:
                    to_send = to_send_task.result()
                    writer.write((to_send + '\n').encode('utf-8', 'ignore'))
                    logger.debug('Sending from %s: %s', self.__class__.__name__, to_send)
                    try:
                        maybe_writer_drain_future = writer.drain()  # funny thing - it's future only if it must wait
                        if isinstance(maybe_writer_drain_future, Future):
                            writer_drain_task = ensure_future(maybe_writer_drain_future)
                            done, pending = await wait(
                                [writer_drain_task, self._finish_future], return_when=FIRST_COMPLETED
                            )
                    except ConnectionResetError:
                        pass
                if self._finish_future in done:
                    for task in pending:
                        task.cancel()
                    break

            try:
                await writer.drain()  # TODO: maybe timeout here?
            except ConnectionResetError:
                pass
        finally:
            writer.close()
            if not self._handle_out_finished_future.done():
                self._handle_out_finished_future.set_result(True)
