import json
import logging
import os
from typing import Dict

from config_watch.config_data import ConfigFileType, ValueWithValidation

logger = logging.getLogger(__name__)


class FileSpec:
    def __init__(self, directory: str, file_type: ConfigFileType, default_config: Dict[str, ValueWithValidation]):
        self.directory = directory
        self.file_type = file_type
        self.file_name = file_type.value
        self.file_path = os.path.join(directory, self.file_name)
        self.default_config = default_config
        self.current_config = {}
        self.config_changed = False

    def get_int(self, key):
        if key in self.current_config:
            return int(self.current_config[key])
        elif key in self.default_config:
            return int(self.default_config[key].initial)
        return None

    def get_str(self, key):
        if key in self.current_config:
            return self.current_config[key]
        elif key in self.default_config:
            return self.default_config[key].initial
        return None

    def save_config(self):
        logger.debug('[%s] Saving config', self.file_name)
        with open(self.file_path, 'w') as f:
            json.dump(self.current_config if len(self.current_config) else self.default_config, f,
                      default=lambda x: x.initial)

    def load_config(self):
        if not os.path.isfile(self.file_path):
            self.save_config()

        invalid_detected = False
        with open(self.file_path, 'r') as f:
            logger.debug('[%s] Loading config', self.file_name)
            data: Dict = json.load(f)

            for k in list(data.keys()):
                if k not in self.default_config:
                    del data[k]

            for k in self.default_config.keys():
                if k not in data:
                    data[k] = self.default_config[k].initial
                else:
                    if not self.default_config[k].validate(data[k]):
                        invalid_detected = True
                        logger.error('[%s] Invalid value detected for key %s: %s', self.file_name, k, data[k])
                        if k in self.current_config and self.default_config[k].validate(self.current_config[k]):
                            data[k] = self.current_config[k]
                        else:
                            data[k] = self.default_config[k].initial

            self.config_changed = data != self.current_config
            self.current_config = data
            logger.debug('[%s] Loaded config: %s', self.file_name, json.dumps(self.current_config))
            if self.config_changed:
                logger.debug('[%s] Config changed', self.file_name)
            if invalid_detected:
                self.save_config()