import asyncio
import logging
import re

logger = logging.getLogger(__name__)


def simple_write(path, val):
    try:
        with open(path, 'w') as fp:
            fp.write(str(val))
    except OSError as e:
        logger.exception('Error writing to "%s" : %s', path, e)


def simple_append(path, val):
    try:
        with open(path, 'w') as fp:
            fp.write(str(val))
    except OSError as e:
        logger.exception('Error appending writing to "%s" : %s', path, e)


def simple_read(path):
    try:
        with open(path, 'r') as fp:
            return fp.readlines()
    except OSError as e:
        logger.exception('Error reading from "%s" : %s', path, e)
        return []


async def simple_run(cmd) -> int:
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()
    logger.debug(f'[{cmd!r} exited with {proc.returncode}]')

    if stdout:
        logger.debug(f'[stdout]\n{stdout.decode()}')
    if stderr:
        logger.debug(f'[stderr]\n{stderr.decode()}')
    return proc.returncode


async def run_and_get_output(cmd, to_write: str = None) -> (str, None):
    proc = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
            stdin=None if to_write is None else asyncio.subprocess.PIPE
    )

    if proc is None:
        logger.error(f'Could not start [{cmd!r}] with input {to_write}')
        return None

    if to_write is not None:
        proc.stdin.write(to_write.encode('ascii', 'ignore'))
        # await proc.stdin.drain()

    stdout, stderr = await proc.communicate()
    logger.debug(f'[{cmd!r} exited with {proc.returncode}]')

    if stdout:
        logger.debug(f'[stdout]\n{stdout.decode()}')
    if stderr:
        logger.debug(f'[stderr]\n{stderr.decode()}')
    return stdout.decode() if stdout else None
