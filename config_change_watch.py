#!/usr/bin/env python3

import asyncio
import logging
import os
import platform
import shlex
import shutil

from config_watch import simple_write, simple_run, run_and_get_output
from config_watch.config_data import ConfigFileType, default_wifi_config, default_eth_config, default_gsm_config
from config_watch.external_process import ExternalProcess
from config_watch.file_spec import FileSpec

try:
    from asyncinotify import asyncinotify as ai
except ImportError:
    import asyncinotify as ai

logger = logging.getLogger(__name__)

pc_debug = not platform.processor() == 'aarch64'


if pc_debug:
    INITIAL_WAIT = 1.0
    WATCH_PATH = '/tmp/dbox_cfg'
else:
    INITIAL_WAIT = 10.0 # in seconds
    WATCH_PATH = '/var/dbox_cfg'


FILE_DATA = [
    FileSpec(WATCH_PATH, ConfigFileType.WIFI, default_wifi_config),
    FileSpec(WATCH_PATH, ConfigFileType.ETH, default_eth_config),
    FileSpec(WATCH_PATH, ConfigFileType.GSM, default_gsm_config),
]

wpa_supplicant_process = None


def recreate_directory():
    try:
        shutil.rmtree(WATCH_PATH)
    except OSError as exc:
        print(f'Error: {exc.filename} - {exc.strerror}.')

    try:
        os.makedirs(WATCH_PATH)
    except OSError as exc:
        print(f'Error: {exc.filename} - {exc.strerror}.')


def recreate_files():
    for f in FILE_DATA:
        try:
            f.load_config()
        except Exception as exc:
            logger.exception(exc)
            f.save_config()
            f.load_config()


def handle_wifi_output(data):
    logger.debug(data)


async def handle_eth_config_change(f: FileSpec):
    is_up = f.get_int('UP') != 0
    is_dhcp = f.get_int('DHCP') != 0
    ip = f.get_str('IP')
    netmask = f.get_str('NETMASK')

    res = await simple_run('/sbin/ip link set eth0 down')

    if res != 0:
        logger.error('Could not set eth link down, res %s', res)

    if is_up:
        res = await simple_run('/sbin/ip link set eth0 up')
        if res != 0:
            logger.error('Could not set eth link up, res %s', res)
        if res == 0:
            if is_dhcp:
                res = await simple_run(f'/sbin/sbin/dhclient eth0')
            else:
                res = await simple_run(f'/sbin/ip a add {ip}/{netmask} dev eth0')
            if res != 0:
                logger.error('Could not set eth IP, res %s', res)



async def handle_gsm_config_change(f: FileSpec):
    pass


async def handle_wifi_config_change(f: FileSpec):
    global wpa_supplicant_process
    wpas_str = """
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1
"""
    is_up = f.get_int('UP') != 0
    is_dhcp = f.get_int('DHCP') != 0
    ip = f.get_str('IP')
    netmask = f.get_str('NETMASK')
    ssid = f.get_str('SSID')
    passwd = f.get_str('PASSWD')
    mode = f.get_str('MODE')    # TODO

    logger.debug('handle_wifi_config_change')

    res = await simple_run('/sbin/ip link set wlan0 down')

    if wpa_supplicant_process is not None:
        await wpa_supplicant_process.kill_process()

    if res != 0:
        logger.error('Could not set WLAN link down, res %s', res)

    if is_up:
        res = await simple_run('/sbin/ip link set wlan0 up')
        if res != 0:
            logger.error('Could not set WLAN link up, res %s', res)
            return

        res = await run_and_get_output(('/usr/bin/wpa_passphrase', ssid, passwd))

        if res is not None:
            simple_write('/tmp/wpa_supplicant.conf', wpas_str + '\n' + res)

            wpa_supplicant_process = ExternalProcess(
                    ('/usr/sbin/wpa_supplicant', '-D', 'wext', '-i', 'wlan0', '-c', '/tmp/wpa_supplicant.conf'),
                    loop=asyncio.get_event_loop(),
                    output_process_function=handle_wifi_output
            )
            wpa_supplicant_process.start()
            await asyncio.sleep(5)

        if res == 0:
            if is_dhcp:
                res = await simple_run(f'/sbin/sbin/dhclient wlan0')
            else:
                res = await simple_run(f'/sbin/ip a add {ip}/{netmask} dev wlan0')
            if res != 0:
                logger.error('Could not set WLAN IP, res %s', res)


async def update_led_blink(led, trigger, delay_on=2.0, delay_off=2.0):
    simple_write(f'/sys/class/leds/{led}/trigger', trigger)
    simple_write(f'/sys/class/leds/{led}/delay_on', int(delay_on*1000))
    simple_write(f'/sys/class/leds/{led}/delay_off', int(delay_off*1000))


async def main():

    if not os.path.exists(WATCH_PATH) or not os.path.isdir(WATCH_PATH):
        recreate_directory()

    recreate_files()

    await asyncio.sleep(1.0)    # wait for everything to stabilize

    await update_led_blink('LED1-Green', 'timer', 2.0, 2.0)

    for f in FILE_DATA:
        if f.file_type == ConfigFileType.ETH:
            await handle_eth_config_change(f)
        elif f.file_type == ConfigFileType.GSM:
            await handle_gsm_config_change(f)
        elif f.file_type == ConfigFileType.WIFI:
            await handle_wifi_config_change(f)

    with ai.Inotify() as inotify:
        # noinspection PyTypeChecker
        inotify.add_watch(WATCH_PATH, ai.Mask.MODIFY | ai.Mask.DELETE | ai.Mask.MOVE | ai.Mask.ONLYDIR)

        async for event in inotify:
            for f in FILE_DATA:
                try:
                    if str(event.path) == f.file_path:
                        try:
                            f.load_config()
                        except Exception as exc:
                            logger.exception(exc)
                        if f.config_changed:
                            if f.file_type == ConfigFileType.ETH:
                                await handle_eth_config_change(f)
                            elif f.file_type == ConfigFileType.GSM:
                                await handle_gsm_config_change(f)
                            elif f.file_type == ConfigFileType.WIFI:
                                await handle_wifi_config_change(f)
                            f.config_changed = False
                except KeyboardInterrupt as exc:
                    logger.exception('Error while handling inotify: %s', exc)
                    raise exc
                except Exception as exc:
                    logger.exception('Error while handling inotify: %s', exc)

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s')
loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(main())
except KeyboardInterrupt as e:
    logger.info('Shutting down')
finally:
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()
